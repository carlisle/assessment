##############################################################################
## assess_files.sh
## Copyright (C) 2019 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
##############################################################################
## Notes
##
##############################################################################

# run all as root unless specified
# examining binary files as text in a shell may scramble text output
# enter the "reset" command to clear
# Not all commands are appropriate on all systems

printf "========================================\n"
printf "= Performing Assessment = $(date +%F) at $(date +%H%M) =\n"
printf "========================================\n"


# packages
# rpm is specific to RedHat family linux
# future goals: add ubuntu and others

  rpm -qa | sort
  rpm -qa --queryformat='%{NAME}.%{arch}\n' | sort

  rpm -Va

# Verify all packages against rpm database - expect false positives when config files have been customized
# output will look like this: S.5....T.  c /etc/ssh/sshd_config
# https://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/RPM_Guide/ch04s04.html
# Code    Meaning
# S    File size differs.
# M    File mode differs.
# 5    The MD5 checksum differs.
# D    The major and minor version numbers differ on a device file.
# L    A mismatch occurs in a link.
# U    The file ownership differs.
# G    The file group owner differs.
# T    The file time (mtime) differs.

# package information using yum
# note: may require network access

# yum
# https://access.redhat.com/sites/default/files/attachments/rh_yum_cheatsheet_1214_jcs_print-1.pdf

# list installed packages and the repositories they came from
yum list installed

# show install package groups
yum grouplist
yum -v grouplist hidden


# aide file integrity checking
# https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-using-aide

# look for aide database
ls -Rl /var/lib/aide
# verify aide database
/usr/sbin/aide --check

# look for tripwire database
ls -Rl /var/lib/tripwire
# tripwire --check

# filesystems

df -h
cat /etc/fstab

printf "List all filesystems: \n\n"
df --local -P | awk {'if (NR!=1) print $6'}


# on rhel7 and above this will be complicated
  cat /etc/mtab

# encrypted luks filesystems
cat /etc/crypttab

# sofware raid
cat /etc/mdadm.conf

# show nfs filesystems and their parameters
  nfsstat -m

# partitions

  fdisk -l
  parted -l

# show block devices
  lsblk

# logical volume manager

# show physical volumes
  pvs
# show logical volumes
  lvs

  pvscan -v
  vgscan -v
  lvscan -v
  vgdisplay -v


# files

# find file by inode
# http://www.linuxask.com/questions/how-to-find-file-by-inode-number

find -inum <inode number>


# look for funny file or directory names
find / -name ...



printf "========================================\n"
printf "Show system executables that don't have root ownership: \n\n"
find /bin/ /usr/bin/ /usr/local/bin/ /sbin/ /usr/sbin/ /usr/local/sbin/ /usr/libexec \! -user root -exec ls -l {} \;

printf "========================================\n"
printf "Show files that differ from expected file hashes\n"
printf "These will report files modified due to hardening: \n\n"
rpm -Va | grep '^..5'

printf "========================================\n"
printf "Find SUID Executables:\n\n"
# Find SUID System Executables
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type f -perm -4000 -print

printf "========================================\n"
printf "Verfiy integrity of the SUID binaries returned by above:\n\n"
SUIDFILES=$(df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type f -perm -4000 -print)
for I in $SUIDFILES; do echo "Integrity of $I:  "; rpm -V $(rpm -qf $I ); echo; done

printf "========================================\n"
printf "Find SGID Executables:\n\n"
# Find SGID System Executables
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type f -perm -2000 -print

printf "========================================\n"
printf "Verfiy integrity of SGID binaries returned by above:\n\n"
SGIDFILES=$(df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' find '{}' -xdev -type f -perm -2000 -print)
for I in $SGIDFILES; do echo "Integrity of $I:  "; rpm -V $(rpm -qf $I ); echo; done

printf "========================================\n"
printf "Verify that All World-Writable Directories Have Sticky Bits Set:\n\n"
# Verify that All World-Writable Directories Have Sticky Bits Set
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type d \( -perm -0002 -a ! -perm -1000 \) 2>/dev/null

printf "========================================\n"
printf "Ensure No World-Writable Files Exist:\n\n"
# Find World Writable Files
# Ensure No World-Writable Files Exist
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type f -perm -0002

printf "========================================\n"
printf "Find Un-owned Files and Directories\n\n"
# Ensure All Files Are Owned by a User
# Find Un-owned Files and Directories
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -nouser -ls

printf "========================================\n"
printf "Find Un-grouped Files and Directories\n\n"
# Ensure All Files Are Owned by a Group
# Find Un-grouped Files and Directories
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -nogroup -ls

# check for core dumps

# core
# core.*
