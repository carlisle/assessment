#!/usr/bin/env sh


HDDTEMP=/usr/sbin/hddtemp
DISK=/dev/sda
CURRENT=0

SEND_EMAIL=1
SEND_SMS=0

#THRESHOLD=65
#THRESHOLD=90
THRESHOLD=97

# https://en.wikipedia.org/wiki/SMS_gateway

EMAIL_LIST=blah@gmail.com
SMS_LIST=555123456@txt.att.net


WARNING_THRESHOLD=88
WARNING_EMAIL_LIST=blah@gmail.com,555123456@txt.att.net


CURRENT=$(${HDDTEMP} -n -u F ${DISK} 


###CURRENT=$(${HDDTEMP} -n -u F ${DISK} | cut -d ' ' -f 10)




if [ "${CURRENT}" -ge "${WARNING_THRESHOLD}" ]
then
	    echo "Temperature Warning: ${CURRENT}" | mail -s "TEMP WARNING! ${CURRENT}" ${WARNING_EMAIL_LIST}
fi


if [ "${CURRENT}" -ge "${THRESHOLD}" ]
then
	    echo "Temperature Too High: ${CURRENT}" | mail -s "TEMP TOO HIGH! ${CURRENT}" ${EMAIL_LIST} 
fi

