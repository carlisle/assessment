##############################################################################
## assess_system.sh
## Copyright (C) 2021 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
##############################################################################
## Notes
##
##############################################################################

# run all as root unless specified
# examining binary files as text in a shell may scramble text output
# enter the "reset" command to clear
# Not all commands are appropriate on all systems

printf "========================================\n"
printf "= Performing System Assessment = $(date +%F) at $(date +%H%M) =\n"
printf "========================================\n"

# disk assessment tools

# https://www.cyberciti.biz/faq/find-hard-disk-hardware-specs-on-linux/


# lsblk

# smartctl  -a /dev/<disk>



# os identification

host=$(hostname) 
OS=$(cat /etc/system-release)
kern=$(uname -r)

# cpu assessment tools

physprocs=$(grep "physical id" /proc/cpuinfo | sort -u  | wc -l)
logprocs=$(grep "processor" /proc/cpuinfo | wc -l)
cores=$(grep "core id" /proc/cpuinfo | sort -u | wc -l)
arch=$(grep "model name" /proc/cpuinfo | uniq | cut -c14-)

#echo $host","$OS","$kern","$physprocs","$logprocs","$cores","$arch


# /proc/cpuinfo

# lscpu

# https://www.binarytides.com/linux-cpu-information/
# https://www.binarytides.com/linux-check-processor/

# spectre-meltdown-checker

# lshw

lshw -short

lshw -short -class system

lshw -short -class memory

lshw -short -class network

lshw -short -class bus


# Other hardware

# hwinfo

# lspci

# inxi
# https://smxi.org/docs/inxi.htm






printf "========================================\n"
printf "= Show Distribution =\n"
printf "========================================\n"
uname -a
cat /etc/os-release
cat /etc/system-release
cat /etc/redhat-release
cat /etc/centos-release
lsb_release -a

