#!/usr/bin/env bash

# References: https://stackoverflow.com/questions/11176284/time-condition-loop-in-shell

_cpus=64
##_stress_time=1200
_stress_time=172800
_delay=30
_max_time=$(( $SECONDS + ${_stress_time}*2 ))

_log_dir=/tmp
_log_file=${_log_dir}/stress-$(hostname)-$(date +%F-%H%M).log

function get_temp {
  sensors
}

printf "\nStress Time: %s\nMax Time: %s\n\n" ${_stress_time} ${_max_time} >> ${_log_file}
printf "Initial State:\n\n%s\n%s\n" "$(hostname)" "$(date +%F-%H%M-%S)" >> ${_log_file}
printf "Total Run Seconds: %s\n\n%s\n\nTemp:\n\n%s\n\n" ${SECONDS} "$(w)" "$(get_temp)"  >> ${_log_file}


#stress --cpu ${_cpus} --timeout ${_stress_time}s >> ${_log_file} 2>> ${_log_file} &

stress-ng --cpu ${_cpus} --timeout ${_stress_time}s >> ${_log_file} 2>> ${_log_file} &

sleep 2


while [ $SECONDS -lt ${_max_time} ]; do

  for (( c=0; c<${_delay}; c++ )); do
     printf "." >> ${_log_file}
     sleep 1
  done

  printf "\n\n%s\n%s\n" "$(hostname)" "$(date +%F-%H%M-%S)" >> ${_log_file}
  printf "Total Run Seconds: %s\n\n%s\n\nTemp:\n\n%s\n" ${SECONDS} "$(w)" "$(get_temp)"  >> ${_log_file}

done


printf "\n\n%s\n%s\n%s" "$(hostname)" "$(date +%F-%H%M-%S)" ${SECONDS} >> ${_log_file}
printf "\n\nDONE!\n" >> ${_log_file}

