#!/usr/bin/env bash

# bonnie++ can not be run as root

TARGETDIR="/mnt/disk1/cgchildr/"
#TARGETDIR=${HOME}
LABEL=$(date +%F-%H%M)
#LOGDIR=${HOME}
LOGDIR="/tmp/"
ERRDIR=${LOGDIR}
LOGFILE=${LOGDIR}/bonnie-${LABEL}.log
ERRFILE=${ERRDIR}/bonnie-${LABEL}.err
EMAILADDR="carlisle.bitbucket@gmail.com"

# User provided parameters ?

#echo ${TARGETDIR} : ${LOGDIR} : ${ERRDIR}
#echo ${LOGFILE} : ${ERRFILE}

# free -h
# df -h

# Check to see if there is enough free space in filesystem

echo $(date +%F-%H%M) > ${LOGFILE}

echo "----"       >> ${LOGFILE}
echo ${TARGETDIR} >> ${LOGFILE}
echo "----"       >> ${LOGFILE}
free -h           >> ${LOGFILE}
echo "----"       >> ${LOGFILE}
df -h             >> ${LOGFILE}
echo "----"     >> ${LOGFILE}
cat /etc/mtab   >> ${LOGFILE}
echo "----"     >> ${LOGFILE}
cat /proc/mounts >> ${LOGFILE}
echo "----"     >> ${LOGFILE}
nfsstat -m      >> ${LOGFILE}
echo "----"     >> ${LOGFILE}

bonnie++ -d ${TARGETDIR} >> ${LOGFILE} 2>> ${ERRFILE}

echo $(date +%F-%H%M) >> ${LOGFILE}

mail -s bonnie-${LABEL}  ${EMAILADDR} < ${LOGFILE}

