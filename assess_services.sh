##############################################################################
## assess_services.sh
## Copyright (C) 2019 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
##############################################################################
## Notes
##
##############################################################################

# run all as root unless specified
# examining binary files as text in a shell may scramble text output
# enter the "reset" command to clear
# Not all commands are appropriate on all systems

printf "========================================\n"
printf "= Performing Assessment = $(date +%F) at $(date +%H%M) =\n"
printf "========================================\n"


# init services ( rhel 6 and before )
  chkconfig --list
  ls -l /etc/rc.d/init.d/

# systemd services ( rhel 7 and above )
# Note: init services can still exist along with systemd services
  systemctl --all
  ls -Rl /etc/systemd/system/

/var/log/messages
/var/log/journal
/var/log/audit


# see current open log files
lsof /var/log


#are these running: rxinetd telnet-server rsh-server ypserv tftp-server
#find -type f -name .rhosts -exec rm -f '{}' \;
#rm /etc/hosts.equiv

# sshd
# check permissions of config files
# /etc/ssh
# audit entries

# Audit sshd config file

printf '%s\n' "Audit /etc/ssh/sshd_config file"
printf '%s\n' "Allow Only SSH Protocol 2"
cat /etc/ssh/sshd_config | grep Protocol

printf '\n%s\n' "Disable GSSAPI Authentication"
cat /etc/ssh/sshd_config | grep GSSAPIAuthentication

printf '\n%s\n' "Disable Kerberos Authentication"
cat /etc/ssh/sshd_config | grep KerberosAuthentication

printf '\n%s\n' "Enable Use of Strict Mode Checking"
cat /etc/ssh/sshd_config | grep StrictModes

printf '\n%s\n' "Disable Compression Or Set Compression to delayed"
cat /etc/ssh/sshd_config | grep Compression

printf '\n%s\n' "Enable Use of Privilege Separation"
cat /etc/ssh/sshd_config | grep UsePrivilegeSeparation

printf '\n%s\n' "Set LogLevel to INFO"
cat /etc/ssh/sshd_config | grep LogLevel

printf '\n%s\n' "MaxAuthTries"
cat /etc/ssh/sshd_config | grep MaxAuthTries

printf '\n%s\n' "Disable Host-Based Authentication"
cat /etc/ssh/sshd_config | grep HostbasedAuthentication

printf '\n%s\n' "Disable SSH Support for .rhosts Files"
cat /etc/ssh/sshd_config | grep IgnoreRhosts

printf '\n%s\n' "Disable SSH Support for User Known Hosts"
cat /etc/ssh/sshd_config | grep IgnoreUserKnownHosts

printf '\n%s\n' "Disable SSH Support for Rhosts RSA Authentication"
cat /etc/ssh/sshd_config | grep RhostsRSAAuthentication

printf '\n%s\n' "Disable SSH Root Login"
cat /etc/ssh/sshd_config | grep PermitRootLogin

printf '\n%s\n' "Disable SSH Access via Empty Passwords"
cat /etc/ssh/sshd_config | grep PermitEmptyPasswords

printf '\n%s\n' "Disable X11 Forwarding, unless needed"
cat /etc/ssh/sshd_config | grep X11Forwarding

printf '\n%s\n' "Do Not Allow SSH Environment Options"
cat /etc/ssh/sshd_config | grep PermitUserEnvironment

printf '\n%s\n' "Set SSH Idle Timeout Interval"
cat /etc/ssh/sshd_config | grep ClientAliveInterval

printf '\n%s\n' "Set SSH Client Alive Count"
cat /etc/ssh/sshd_config | grep ClientAliveCountMax

printf '\n%s\n' "Enable SSH Warning Banner"
cat /etc/ssh/sshd_config | grep Banner

printf '\n%s\n' "Print Last Log"
cat /etc/ssh/sshd_config | grep PrintLastLog

printf '\n%s\n' "Limit allowed users, if possible"
cat /etc/ssh/sshd_config | grep AllowUsers

printf '\n%s\n' "Limit allowed groups, if possible"
cat /etc/ssh/sshd_config | grep AllowGroups

printf '\n%s\n' "Deny certain users, if possible"
cat /etc/ssh/sshd_config | grep DenyUsers

printf '\n%s\n' "Deny certain groups, if possible"
cat /etc/ssh/sshd_config | grep DenyGroups

printf '\n%s\n' "Disable CBC Mode Ciphers"

sshd -T | grep cipher | grep -i cbc

cat /etc/ssh/sshd_config | grep Ciphers

printf '\n%s\n' "Disable any 96-bit HMAC Algorithms.Disable any MD5-based HMAC Algorithms"

sshd -T | grep mac | grep -i HMAC

sshd -T | grep mac | grep -i md5

cat /etc/ssh/sshd_config | grep MACs

# diffie-hellman-group1-sha1

sshd -T | grep kex | grep -i diffie-hellman-group1-sha1

# Audit ssh_config file
printf '\n%s\n' "#### Audit ssh_config file ####"
printf '\n%s\n' "Allow Only SSH Protocol 2"
cat /etc/ssh/ssh_config | grep Protocol

printf '\n%s\n' "Enable HashKnownHosts to obscure destination servers"
cat /etc/ssh/ssh_config | grep HashKnownHosts

# Audit sshd algorithms
# https://access.redhat.com/discussions/2143791
# https://serverfault.com/questions/758673/how-to-disable-diffie-hellman-group1-sha1-for-ssh

sshd -T | grep cipher

sshd -T | grep ciper-auth

sshd -T | grep mac

# Key Exchange Algorithms
sshd -T | grep kex

sshd -T | grep key

# nmap

# https://nmap.org/book/nse-usage.html
#https://www.techrepublic.com/article/security-auditing-with-nmap/

# https://isc.sans.edu/diary/Auditing+SSH+Settings+%28some+Blue+Team%2C+some+Red+Team%29/22998


# Audit ssh client algorithms

ssh -Q cipher

ssh -Q cipher-auth

ssh -Q mac

# Key Exchange Algorithms
ssh -Q kex

ssh -Q key



# tls certs
# check permissions
# /etc/pki/tls

#  read and verify certificates

openssl x509 -in /etc/pki/tls/certs/*.crt -noout -text



# httpd
# check permissions of config files
# /var/www/html

# check status

service httpd status
systemctl status httpd
apachectl status

# The location of the certificate and key files according to the configuration file:

grep ^SSLCertificateFile /etc/httpd/conf.d/ssl.conf | awk '{print $2}'
grep ^SSLCertificateKeyFile /etc/httpd/conf.d/ssl.conf | awk '{print $2}'

# test connection to httpd server manually

telnet <doain name> 80

send:
GET / HTTP/1.1
Host: <domain name>
User-Agent: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)

<press enter twice>


# test tls connection

openssl s_client -connect <domain name>:443

send:
GET / HTTP/1.1
Host: <domain name>


echo 'q' | openssl s_client -host <domain name> -port 443


# show ssl certificates
openssl s_client -host <domain name> -port 443 -showcerts


# mysql

# Can you log into mysql database
mysql -u root -h localhost

# find mysql stored passwords
# use of locate requires the mlocate database to be up-to-date
locate my.cnf
or
find /root -name .my.cnf
find /home -name .my.cnf



# mail servers


# test connection to smtp server manually

telnet <server ip> 25

send: ehlo localhost.localdomain

# postfix
# configuration: /etc/postfix/main.cf

# look for open relay
# most servers do not need to be an open relay for all addresses
cat /etc/postfix/main.cf | grep mynetworks

# relay host
# all mail is forwarded to another mail server
# this is usually a good thing to do, until the receiving server isn't trusted
cat /etc/postfix/main.cf | grep relayhost


# sendmail
# configuration files under /etc/mail

# open relay
cat /etc/mail/access | grep -i relay

# relay host
cat /etc/mail/sendmail.mc | grep -i SMART_HOST 

# Backups

# See when Tivoli Storage Manager / IBM Spectrum Protect was last run
dsmc q fi
