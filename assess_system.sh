##############################################################################
## assess_system.sh
## Copyright (C) 2019 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
##############################################################################
## Notes
##
##############################################################################

# run all as root unless specified
# examining binary files as text in a shell may scramble text output
# enter the "reset" command to clear
# Not all commands are appropriate on all systems

printf "========================================\n"
printf "= Performing System Assessment = $(date +%F) at $(date +%H%M) =\n"
printf "========================================\n"

printf "========================================\n"
printf "= Show Distribution =\n"
printf "========================================\n"
uname -a
cat /etc/os-release
cat /etc/system-release
cat /etc/redhat-release
cat /etc/centos-release
lsb_release -a

printf "========================================\n"
printf "= Display uptime, Load, Current Logged in users =\n"
printf "========================================\n"
w

printf "========================================\n"
printf "= Networking  =\n"
printf "========================================\n"

System_Hostname=$(hostname)

printf "Hostname: %s\n" ${System_Hostname}

System_Ip=$(host ${System_Hostname} | awk '/has address/ { print $4 }')

printf "Ip for Hostname: %s\n" ${System_Ip}

printf "Reverse Lookup for Hostname: " 
host  ${System_Ip}

printf "Whois for ip address for hostname: \n"
whois ${System_Ip}


# net-tools systems rhel6 or below

printf "Show All Interfaces: \n"
ifconfig -a
printf "Show All routes: \n"
route
printf "Show Active Internet Connections: \n"
netstat -tunap

# iproute systems rhel7 and above
printf "Show All Interfaces: \n"
ip a
printf "Show All routes: \n"
ip route
printf "Show Active Internet Connections: \n"
ss -tunap

printf "Show System DNS Servers: =\n"
cat /etc/resolv.conf

# kernel information
uname -a
lsmod

# kernel parameters

# check configuration files for persistent settings

cat /etc/sysctl.conf
cat /etc/sysctl.d/*
cat /usr/lib/sysctl.d/*
cat /run/sysctl.d/*

# check sysctl for current settings

printf "========================================\n"
printf "sysctl configuration:\n"
printf "The following should be set to 0:\n\n"

sysctl --all | grep net.ipv4.conf.default.send_redirects
sysctl --all | grep net.ipv4.conf.all.send_redirects
sysctl --all | grep "net.ipv4.ip_forward "
sysctl --all | grep net.ipv4.conf.all.accept_source_route
sysctl --all | grep net.ipv4.conf.all.accept_redirects
sysctl --all | grep net.ipv4.conf.all.secure_redirects
sysctl --all | grep fs.suid_dumpable

printf "\nThe following should be set to 1:\n\n"
sysctl --all | grep net.ipv4.conf.all.log_martians
sysctl --all | grep net.ipv4.conf.default.log_martians
sysctl --all | grep net.ipv4.icmp_echo_ignore_broadcasts
sysctl --all | grep net.ipv4.icmp_ignore_bogus_error_responses

printf "\nThe following should be set to 0:\n\n"
sysctl --all | grep net.ipv4.conf.default.accept_source_route
sysctl --all | grep net.ipv4.conf.all.accept_redirects
sysctl --all | grep net.ipv4.conf.default.secure_redirects
#sysctl --all | grep net.ipv4.icmp_echo_ignore_broadcasts
#sysctl --all | grep net.ipv4.icmp_ignore_bogus_error_responses
sysctl --all | grep net.ipv4.tcp_syncookies
sysctl --all | grep net.ipv4.conf.all.rp_filter
sysctl --all | grep net.ipv4.conf.default.rp_filter
sysctl --all | grep net.ipv6.conf.all.disable_ipv6
sysctl --all | grep "net.ipv6.conf.all.accept_ra "
sysctl --all | grep "net.ipv6.conf.default.accept_ra "
sysctl --all | grep net.ipv6.conf.all.accept_redirects
sysctl --all | grep net.ipv6.conf.default.accept_redirects

printf "========================================\n"
printf "other\n\n"



# this output will be large
dmesg

# selinux state
# specific to RedHat family linux 
getenforcing

# look for selinux deny statements in audit log

grep denied /var/log/audit/audit.log

# chkrootkit

# lynis

# Memory

free -h

# processes

  ps faux

# list all open files
# output will be large
  lsof

# list open file for a given process
  lsof -p <pid>

# Find processes that are accessing deleted files
# https://unix.stackexchange.com/questions/68523/find-and-remove-large-files-that-are-open-but-have-been-deleted

  lsof -nP | grep '(deleted)'

# one iteration of top
  top n1

# N interations of iotop
  iotop -botqqqk --iter=N

# /proc filesystem
# Note: /proc/kcore is the system memory and is as big as the amount of system ram
# don't try to read or copy it

# show process data within /proc for each process

ls -larthd /proc/[1-9]*

ls -larth /proc/[1-9]*


# hardware
  lspci -vv
  lshw
  lsblk

# may be obsolete
##  smoltSendProfile -p

# Cron and anacron jobs
# look for jobs that remove files or
# enable accounts

# What cron jobs are installed
ls -larth /var/spool/cron/*

# What anacron jobs are installed
ls -larth ls -larth /etc/cron*


# logs

cat /var/log/messages
cat /var/log/secure
cat /var/log/system

# sysstat
# sar

# ossec

# accounts configured in ossec
grep -E "${NAME1}|${NAME2}" /var/ossec/etc/*.conf

grep -E "${NAME1}|${NAME2}" /usr/local/ossec/etc/*.conf


# See logwatch configuration
cat /etc/logwatch/conf/logwatch.conf

# Firewall

# firewalld

# iptables
# see all current iptables rules for each table

iptables -vL -t filter
iptables -vL -t nat
iptables -vL -t mangle
iptables -vL -t raw
iptables -vL -t security
ip6tables -vL -t filter
ip6tables -vL -t nat
ip6tables -vL -t mangle
ip6tables -vL -t raw
ip6tables -vL -t security

# note if iptables is logging any packets, look for LOG entries

# if installed, will show active filewall activity
iptstate


# openscap
# https://www.open-scap.org/
#

# if installed,

# Profiles are usually installed in /usr/share/xml/scap/ssg/content

# example for rhel7 and disa stig ( Defense Information Systems Agency Security Technical Implementation Guides  ) profile
oscap xccdf eval -fetch-remote-resources  --profile xccdf_org.ssgproject.content_profile_stig-rhel7-disa  \
     --results oscap_results.xml \
     --report  oscap_report.html \
     /usr/share/xml/scap/ssg/content/ssg-rhel7-xccdf.xml;

