##############################################################################
## assess_accounts.sh
## Copyright (C) 2020 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
##############################################################################
## Notes
##
##############################################################################

# run all as root unless specified
# examining binary files as text in a shell may scramble text output
# enter the "reset" command to clear
# Not all commands are appropriate on all systems

printf "========================================\n"
printf "= Performing Assessment = $(date +%F) at $(date +%H%M) =\n"
printf "========================================\n"


# accounts and passwords

printf "/etc/passwd* files\n"
ls -lh /etc/passwd*
cat /etc/passwd*

printf "\n/etc/group* files\n"
ls -lh /etc/group*
cat /etc/group*

printf "\n/etc/shadow* files\n"
ls -lh /etc/shadow*
cat /etc/shadow*

printf "\n/etc/gshadow* files\n"
ls -lh /etc/gshadow*
cat /etc/gshadow*

printf "\n/etc/security/opasswd file\n"
ls -lh /etc/security/opasswd
cat /etc/security/opasswd

printf "========================================\n"
printf "Verify integrity of passwd, shadow, and group files\n\n"
# Verify integrity of passwd shadow and group files

pwck -r
grpck -r


who
last
lastlog

# Locked accounts are designated by '!!' at the begining of the hash in /etc/shadow entry
# but locked accounts are still accessible by ssh key authentication if a key is in ~/.ssh



# search for specific account names in various places
# declare a bash array and iterate through that in a loop

declare -a Search_Array=(
name1
name2
etc
)

for Search_Entry in "${Search_Array[@]}"; do

  printf "\nAccounts:  %s\n" ${Search_Entry}
  cat /etc/passwd | grep -E ${Search_Entry}
# more later
# check tls certificates

  Search_Entry_Home=$(getent passwd ${Search_Entry} | cut -d: -f6)
  printf "\nHome Directory: %s\n" ${Search_Entry}
  printf "\n%s\n" ${Search_Entry_Home}

  printf "\nPassword Hashes:  %s\n" ${Search_Entry}
  cat /etc/shadow | grep -E ${Search_Entry}

  printf "\nPassword Status: %s\n" ${Search_Entry}
  passwd -S ${Search_Entry}

  printf "\nGroups:  %s\n" ${Search_Entry}
  cat /etc/group | grep -E ${Search_Entry}

  printf "\nAccounts with sudo privledges:  %s\n" ${Search_Entry}
  cat /etc/sudoers | grep -E ${Search_Entry}
  cat /etc/group   | grep -E wheel | grep -E ${Search_Entry}


  printf "\nLogin Entries: %s\n" ${Search_Entry}
  cat /var/log/secure | grep -E ${Search_Entry}

  printf "\nLast:  %s\n" ${Search_Entry}
  last | grep -E ${Search_Entry}

  printf "\nLastlog:  %s\n" ${Search_Entry}
  lastlog | grep -E ${Search_Entry}

  printf "\nProcesses: %s\n" ${Search_Entry}
  ps -ef | grep -E ${Search_Entry}

  # ssh keys
  ls -al ${Search_Entry_Home}/.ssh

  # cronjobs
  crontab -u ${Search_Entry} -l

  # mail
  ls -al /var/mail/${Search_Entry}

  printf "\n%s\n" "----"
done

printf "\nWho's in the wheel group\n"
grep wheel /etc/group


# On rhel4, 5, & 6 FIRSTUSER=500
# On rhel7 & 8 FIRSTUSER=1000
FIRSTUSER=1000


printf "========================================\n"
printf "Ensure All World-Writable Directories Are Owned by a System Account\n\n"
# Ensure All World-Writable Directories Are Owned by a System Account
# Assumes system accounts have uid < $FIRSTUSER
df --local -P | awk {'if (NR!=1) print $6'} | xargs -I '{}' \
find '{}' -xdev -type d -perm -0002 -uid +$FIRSTUSER -print


printf "========================================\n"
printf "Ensure Password Fields are Not Empty\n\n"
# Ensure Password Fields are Not Empty
/bin/cat /etc/shadow | /bin/awk -F: '($2 == "" ) { print $1 " does not have a password "}' 

printf "========================================\n"
printf "Check each user directory for .rhosts, .netrc, or .forward files\n\n"
# CIS 9.2.10 Check for Presence of User .rhosts Files
# CIS 9.2.18 Check for Presence of User .netrc Files
# CIS 9.2.19 Check for Presence of User .forward Files
HOMEDIR=$(/bin/cat /etc/passwd | /bin/egrep -v '(root|halt|sync|shutdown)' \
 | /bin/awk -F: '($7 != "/sbin/nologin") { print $6 }')
for file in ${HOMEDIR}/.rhosts; do
    if [ ! -h "${FILE}" -a -f "${FILE}" ]; then
      echo ".rhosts file ${HOMEDIR}/.rhosts exists"
      echo ".netrc file ${HOMEDIR}/.netrc exists"
      echo ".forward file ${HOMEDIR}/.forward exists"
    fi 
done


