#!/usr/bin/env bash

# create 100x1GB files
for i in {1..100}; do dd if=/dev/zero of=1G$i.zero count=131072 bs=8192 &> /dev/null ; done &
