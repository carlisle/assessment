##############################################################################
## remote_scanning.sh
## Copyright (C) 2021 Carlisle Childress 
## carlisle.bitbucket@gmail.com
##
##############################################################################
## Files modified
##
############################################################################
## License
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
##############################################################################
## References
##
##############################################################################
## Notes
##
##############################################################################

# run all as root unless specified
# examining binary files as text in a shell may scramble text output
# enter the "reset" command to clear
# Not all commands are appropriate on all systems

printf "========================================\n"
printf "= Performing Assessment = $(date +%F) at $(date +%H%M) =\n"
printf "========================================\n"



# nmap

# https://nmap.org/book/nse-usage.html
#https://www.techrepublic.com/article/security-auditing-with-nmap/

# https://isc.sans.edu/diary/Auditing+SSH+Settings+%28some+Blue+Team%2C+some+Red+Team%29/22998





# tls certs
# check permissions
# /etc/pki/tls

#  read and verify certificates



# httpd


openssl s_client -connect <domain name>:443

send:
GET / HTTP/1.1
Host: <domain name>


echo 'q' | openssl s_client -host <domain name> -port 443


# show ssl certificates
openssl s_client -host <domain name> -port 443 -showcerts


# mysql



# mail servers


# test connection to smtp server manually

telnet <server ip> 25

send: ehlo localhost.localdomain

