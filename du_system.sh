#!/usr/bin/env bash

IONICE="ionice -c2 -n7"
NICE="nice -n 18"
LOGSDIR="/tmp/du-home"
DEPTH=3
START_DATE=$(date +%F)


# start timer
START=$(date +%s)
NSTART=$(date +%s.%N)


# for testing
declare -a DIR_ARRAY=( home etc var usr/local ) 

for DIR in "${DIR_ARRAY[@]}"; do
	
	LOGS=${LOGSDIR}/du-${DIR}-$(date +%F).log ${START_DATE}.log
	ERRS=${LOGSDIR}/du-${DIR}-$(date +%F).err ${START_DATE}.err

	touch ${LOGS}
	touch ${ERRS}

	${IONICE} ${NICE} du -k  --max-depth=${DEPTH} /${DIR}/ > ${LOGS} 2> ${LOGS} 2> ${ERRS} 

done

COMBS=${LOGSDIR}/du-combined-${START_DATE}.log
SORTS=${LOGSDIR}/du-${DIR}-${START_DATE}.sorted.list

touch ${COMBS}
touch ${SORTS}

cat ${LOGSDIR}/du-*-${START_DATE}.log > ${COMBS}
cat ${COMBS}  | sort -rn >  ${SORTS} 


# End timer

END=$(date +%s)
NEND=$(date +%s.%N)
 
let TOTAL="${END} - ${START}"
DIFF=$(echo "${NEND} - ${NSTART}" | bc)
 
echo "Script took ${TOTAL} seconds"
echo "Script tool ${DIFF} Nanoseconds"

